# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: 1.3~rc2-1-ga15645d\n"
"PO-Revision-Date: 2015-02-23 11:37+0200\n"
"Last-Translator: Michal Čihař <michal@cihar.com>\n"
"Language-Team: Armenian <https://hosted.weblate.org/projects/noosfero/plugin-"
"shopping-cart/hy/>\n"
"Language: hy\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Weblate 2.3-dev\n"

#: ../controllers/shopping_cart_plugin_controller.rb:138
msgid ""
"Your order has been sent successfully! You will receive a confirmation e-"
"mail shortly."
msgstr ""

#: ../controllers/shopping_cart_plugin_controller.rb:159
#, fuzzy
msgid "Basket displayed."
msgstr "Մենյուի մեջ ցույց չտալ"

#: ../controllers/shopping_cart_plugin_controller.rb:178
msgid "Basket hidden."
msgstr ""

#: ../controllers/shopping_cart_plugin_controller.rb:202
#, fuzzy
msgid "Delivery option updated."
msgstr "Վերացնել անհատական էջը"

#: ../controllers/shopping_cart_plugin_controller.rb:220
msgid ""
"Your basket contains items from '%{profile_name}'. Please empty the basket "
"or checkout before adding items from here."
msgstr ""

#: ../controllers/shopping_cart_plugin_controller.rb:234
#, fuzzy
msgid "There is no basket."
msgstr "Այստեղ կատեգորիաներ չկան:"

#: ../controllers/shopping_cart_plugin_controller.rb:250
#, fuzzy
msgid "This enterprise doesn't have this product."
msgstr "Այս մասնակիցն աշխատում է հետևյալ ձեռնարկություններում"

#: ../controllers/shopping_cart_plugin_controller.rb:264
#, fuzzy
msgid "The basket doesn't have this product."
msgstr "Այս մասնակիցն աշխատում է հետևյալ ձեռնարկություններում"

#: ../controllers/shopping_cart_plugin_controller.rb:278
msgid "Invalid quantity."
msgstr ""

#: ../controllers/shopping_cart_plugin_controller.rb:367
#, fuzzy
msgid "Undefined product"
msgstr "Չկատեգորիզացված արտադրանք"

#: ../controllers/shopping_cart_plugin_controller.rb:369
#, fuzzy
msgid "Wrong product id"
msgstr "Արտադրանք չկա"

#: ../lib/shopping_cart_plugin.rb:8
#, fuzzy
msgid "A shopping basket feature for enterprises"
msgstr "Դիզակտիվացնել ձեռնարկությունների որոնումը"

#: ../lib/shopping_cart_plugin.rb:31
msgid "Shopping"
msgstr ""

#: ../lib/shopping_cart_plugin/cart_helper.rb:11
msgid "Add to basket"
msgstr ""

#: ../lib/shopping_cart_plugin/control_panel/shopping_preferences.rb:6
msgid "Preferences"
msgstr ""

#: ../lib/shopping_cart_plugin/mailer.rb:20
#, fuzzy
msgid "[%s] Your buy request was performed successfully."
msgstr "Ձեր գաղտնաբառը բարեհաջող փոխվել է:"

#: ../lib/shopping_cart_plugin/mailer.rb:36
msgid "[%s] You have a new buy request from %s."
msgstr ""

#: ../views/public/_cart.html.erb:6 ../views/public/_cart.html.erb:19
#: ../views/shopping_cart_plugin/buy.html.erb:2
msgid "Shopping checkout"
msgstr ""

#: ../views/public/_cart.html.erb:8
#, fuzzy
msgid "Basket is empty"
msgstr "Մենյուի մեջ ցույց չտալ"

#: ../views/public/_cart.html.erb:14
msgid "Basket"
msgstr ""

#: ../views/public/_cart.html.erb:16
#, fuzzy
msgid "Clean basket"
msgstr "Ստեղծված է"

#: ../views/public/_cart.html.erb:20
#: ../views/shopping_cart_plugin/_items.html.erb:48
msgid "Total:"
msgstr ""

#: ../views/public/_cart.html.erb:23
#, fuzzy
msgid "Show basket"
msgstr "Որոնել"

#: ../views/public/_cart.html.erb:24
msgid "Hide basket"
msgstr ""

#: ../views/public/_cart.html.erb:44
msgid "Ups... I had a problem to load the basket list."
msgstr ""

#: ../views/public/_cart.html.erb:46
#, fuzzy
msgid "Did you want to reload this page?"
msgstr "Ցանկանո՞ւմ եք արդյոք հեռացնել այս մեկնաբանությունը:"

#: ../views/public/_cart.html.erb:49
msgid "Sorry, you can't have more then 100 kinds of items on this basket."
msgstr ""

#: ../views/public/_cart.html.erb:51
msgid "Oops, you must wait your last request to finish first!"
msgstr ""

#: ../views/public/_cart.html.erb:52
#, fuzzy
msgid "Are you sure you want to remove this item?"
msgstr "Վստա՞ք եք, որ ցականում եք այն հեռացնել:"

#: ../views/public/_cart.html.erb:53
#, fuzzy
msgid "Are you sure you want to clean your basket?"
msgstr "Վստա՞հ եք, որ ցանկանում եք դուրս գալ:"

#: ../views/public/_cart.html.erb:54
msgid "repeat order"
msgstr ""

#: ../views/shopping_cart_plugin/_items.html.erb:7
msgid "Item"
msgstr ""

#: ../views/shopping_cart_plugin/_items.html.erb:10
msgid "Qtty"
msgstr ""

#: ../views/shopping_cart_plugin/_items.html.erb:13
msgid "Unit price"
msgstr ""

#: ../views/shopping_cart_plugin/_items.html.erb:16
msgid "Total"
msgstr ""

#: ../views/shopping_cart_plugin/buy.html.erb:3
#: ../views/shopping_cart_plugin/buy.html.erb:46
msgid "haven't finished yet: back to shopping"
msgstr ""

#: ../views/shopping_cart_plugin/buy.html.erb:10
msgid "Your Order"
msgstr ""

#: ../views/shopping_cart_plugin/buy.html.erb:18
msgid "Personal identification"
msgstr ""

#: ../views/shopping_cart_plugin/buy.html.erb:21
msgid "Name"
msgstr "Անվանում"

#: ../views/shopping_cart_plugin/buy.html.erb:22
#, fuzzy
msgid "Email"
msgstr "Էլ. հասցե"

#: ../views/shopping_cart_plugin/buy.html.erb:23
msgid "Contact phone"
msgstr ""

#: ../views/shopping_cart_plugin/buy.html.erb:28
#: ../views/shopping_cart_plugin/mailer/customer_notification.html.erb:22
msgid "Payment's method"
msgstr ""

#: ../views/shopping_cart_plugin/buy.html.erb:31
#: ../views/shopping_cart_plugin/mailer/supplier_notification.html.erb:20
msgid "Payment"
msgstr ""

#: ../views/shopping_cart_plugin/buy.html.erb:32
#: ../views/shopping_cart_plugin/mailer/customer_notification.html.erb:24
#: ../views/shopping_cart_plugin/mailer/supplier_notification.html.erb:22
#, fuzzy
msgid "shopping_cart|Change"
msgstr "Վերացնել անհատական էջը"

#: ../views/shopping_cart_plugin/buy.html.erb:38
#, fuzzy
msgid "Delivery or pickup method"
msgstr "Վերացնել անհատական էջը"

#: ../views/shopping_cart_plugin/buy.html.erb:47
msgid "Send buy request"
msgstr ""

#: ../views/shopping_cart_plugin/mailer/customer_notification.html.erb:7
#: ../views/shopping_cart_plugin/mailer/supplier_notification.html.erb:7
msgid "Hi %s!"
msgstr ""

#: ../views/shopping_cart_plugin/mailer/customer_notification.html.erb:10
msgid ""
"This is a notification e-mail about your buy request on the enterprise %s."
msgstr ""

#: ../views/shopping_cart_plugin/mailer/customer_notification.html.erb:11
msgid ""
"The enterprise already received your buy request and will contact you for "
"confirmation."
msgstr ""

#: ../views/shopping_cart_plugin/mailer/customer_notification.html.erb:12
msgid "If you have any doubts about your order, write to us at: %s."
msgstr ""

#: ../views/shopping_cart_plugin/mailer/customer_notification.html.erb:13
msgid "Review below the informations of your order:"
msgstr ""

#: ../views/shopping_cart_plugin/mailer/customer_notification.html.erb:19
#: ../views/shopping_cart_plugin/mailer/supplier_notification.html.erb:17
#, fuzzy
msgid "Phone number"
msgstr "Նոր անդամ"

#: ../views/shopping_cart_plugin/mailer/customer_notification.html.erb:28
#, fuzzy
msgid "Delivery or pickup"
msgstr "Վերացնել անհատական էջը"

#: ../views/shopping_cart_plugin/mailer/customer_notification.html.erb:63
msgid "Here are the products you bought:"
msgstr ""

#: ../views/shopping_cart_plugin/mailer/customer_notification.html.erb:67
#, fuzzy
msgid "Thanks for buying with us!"
msgstr "Շնորհակալություն գրանցվելու համար:"

#: ../views/shopping_cart_plugin/mailer/customer_notification.html.erb:70
#: ../views/shopping_cart_plugin/mailer/supplier_notification.html.erb:61
msgid "A service of %s."
msgstr ""

#: ../views/shopping_cart_plugin/mailer/supplier_notification.html.erb:10
msgid "This is a buy request made by %s."
msgstr ""

#: ../views/shopping_cart_plugin/mailer/supplier_notification.html.erb:11
msgid "Below follows the customer informations:"
msgstr ""

#: ../views/shopping_cart_plugin/mailer/supplier_notification.html.erb:55
msgid "And here are the items bought by this customer:"
msgstr ""

#: ../views/shopping_cart_plugin/mailer/supplier_notification.html.erb:59
msgid "If there are any problems with this email contact the admin of %s."
msgstr ""

#: ../views/shopping_cart_plugin_myprofile/edit.html.erb:1
#, fuzzy
msgid "Basket options"
msgstr "Կառավարման հատկություններ"

#: ../views/shopping_cart_plugin_myprofile/edit.html.erb:7
msgid "Enable shopping basket"
msgstr ""

#: ../views/shopping_cart_plugin_myprofile/edit.html.erb:13
#, fuzzy
msgid "Deliveries or pickups"
msgstr "Վերացնել անհատական էջը"

#: ../views/shopping_cart_plugin_myprofile/edit.html.erb:18
msgid "Back to control panel"
msgstr ""
